#!/usr/bin/env node
import * as cdk from '@aws-cdk/core';
import { CdkPocStack } from '../lib/cdk-poc-stack';

const app = new cdk.App();
new CdkPocStack(app, 'CdkPocStack');
