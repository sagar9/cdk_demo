import lambda = require('@aws-cdk/aws-lambda');
import * as cdk from '@aws-cdk/core';
import apigw = require('@aws-cdk/aws-apigateway');
import { CfnDocumentationPart, CfnDocumentationVersion } from '@aws-cdk/aws-apigateway';

export class CdkPocStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const sumLmabda = new lambda.Function(this, 'SumHandler', {
      functionName: "cdk_demo_sum",
      runtime: lambda.Runtime.NODEJS_12_X,
      code: lambda.Code.fromAsset('lambda_1'),
      handler: 'src/sum.handler',
    });

    // const lambdaRestApi = new apigw.LambdaRestApi(this, 'Endpoint', {
    //   handler: sumLmabda,
    //   deployOptions: {
    //     stageName: "dev",
    //   },
    //   defaultMethodOptions: {
    //     apiKeyRequired: false,
    //   }
    // });

    const api = new apigw.RestApi(this, 'sumApi', {
      restApiName: 'Sum Service',
      deployOptions: {
        stageName: "dev",
      },
      defaultMethodOptions: {
        apiKeyRequired: false
      }
    });

    const cal = api.root.addResource('cal');
    const sumResource = cal.addResource("sum");
    const lambdaIntegration = new apigw.LambdaIntegration(sumLmabda);
    
    sumResource.addMethod('GET', lambdaIntegration);

    let prop = {
      "info": {
        "description": "My first API with AWS CDK."
      },
    };

    let docPrt = new CfnDocumentationPart(this, "doc-part",  {
      location: {
        method: "GET",
        path: "calc/sum",
        type: "METHOD"
      },
      properties: JSON.stringify(prop),
      restApiId: api.restApiId,
    });

    let docVersion = new CfnDocumentationVersion(this, "doc-version", {
      description: "version 1",
      documentationVersion: "1",
      restApiId: api.restApiId
    });
  }
}
